%include "lib.inc"

%define CONST 8

global find_word

section .text

find_word:
    push rsi

    .loop:
        mov r12, rsi
        lea rsi, [rsi+CONST]
   
        call string_equals
        test rax, rax
        jz .next

        mov rax, r12
        pop rsi
        ret

    .next:
	mov rsi, [r12]
	test rsi, rsi
	jnz .loop
	xor rax, rax
	pop rsi
	ret