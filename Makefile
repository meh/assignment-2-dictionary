.PHONY: clean test

ASM = nasm
AF = -f elf64
ASM_FILES = main.asm lib.asm dict.asm
OBJ_FILES = $(ASM_FILES:%.asm=%.o)
INC_FILES = words.inc lib.inc dict.inc

words.inc: colon.inc
lib.o: lib.asm
dict.o: dict.asm lib.inc
main.asm: $(INC_FILES)
main.o: main.asm $(INC_FILES)

main: $(OBJ_FILES)
	ld -o $@ $+
	make clean

%.o: %.asm
	$(ASM) $(AF) -o $@ $<

clean:
	rm -f *.o

test:
	make main
	python3 test.py
	rm -f main