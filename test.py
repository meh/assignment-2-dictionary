import subprocess

cases = [
    {'input': '21102910', 'expected_output': 'non-digital responsee', 'expected_error': ''},
    {'input': 'firstword', 'expected_output': 'fr fr', 'expected_error': ''},
    {'input': 'dictionary_in_assembly_deadline', 'expected_output': '16.10', 'expected_error': ''},
    {'input': 'errortest', 'expected_output': '', 'expected_error': 'Error: string is not foundd'},
    {'input': ':)' * 130, 'expected_output': '', 'expected_error': 'Error: read error'},
    {'input': 'a' * 255, 'expected_output': 'b', 'expected_error': ''},
    {'input': 'b' * 256, 'expected_output': '', 'expected_error': 'Error: read error'},
]

for case in cases:
    process = subprocess.run("./main", input=case['input'], text=True, capture_output=True)

    stdout = process.stdout[11:-1]
    stderr = process.stderr[:-1]

    if (stdout != case['expected_output'] or stderr != case['expected_error']):
        print("--------------------")
        print("\033[31m {}\033[0m" .format("[ERROR]"))
        print("Input:", repr(case['input']))
        print("Expected Output:", repr(case['expected_output']))
        print("Expected Error:", repr(case['expected_error']))
        print("Actual Output:", repr(stdout))
        print("Actual Error:", repr(stderr))
        print("--------------------")
    else:
        print("\033[32m {}\033[0m" .format("[OK]") + ' ' + str(case['input']))
