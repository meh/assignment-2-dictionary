%define first 0

%macro colon 2

%ifnstr %1
	%error "Error: first argument of the 'colon' macro is not a string!"
%endif
%ifnid %2
	%error "Error: second argument of the 'colon' macro is not a label!"
%endif

%2:
	dq first
	db %1, 0

%define first %2

%endmacro