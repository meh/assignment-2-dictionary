%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define PTR first_pointer
%define ERROR_EXIT_CODE 1
%define CONST 8

%define BUF_SIZE 256

section .rodata:
    entry_text: db "Enter key: ", 0
    errmsg1: db "Error: read error", 10, 0
    errmsg2: db "Error: string is not found", 10, 0

section .bss
    input: resb BUF_SIZE

section .text
global _start

_start:
    mov rdi, entry_text
    call print_string

    mov rdi, input
    mov rsi, BUF_SIZE-1
    call read_word
    test rax, rax
    jnz .next

    mov rdi, errmsg1

.error:
    call print_error_message
    mov rdi, ERROR_EXIT_CODE
    call exit

.next:
    mov rsi, PTR
    mov rdi, rax
    call find_word
    test rax, rax
    mov rdi, errmsg2
    jz .error

    mov rdi, rax
    add rdi, CONST
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi

    call print_string
    call print_newline
    xor rdi, rdi
    call exit